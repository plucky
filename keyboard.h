#ifndef __keyboard_h__
#define __keyboard_h__

#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>

#include "usbdrv.h"
#include "usb_hid_keys.h"

static uchar idle_rate; // in 4 ms units

/* We use a simplifed keyboard report descriptor which does not support the
 * boot protocol. We don't allow setting status LEDs and but we do allow
 * simultaneous key presses.
 * The report descriptor has been created with usb.org's "HID Descriptor Tool"
 * which can be downloaded from http://www.usb.org/developers/hidpage/.
 * Redundant entries (such as LOGICAL_MINIMUM and USAGE_PAGE) have been omitted
 * for the second INPUT item.
 */
const PROGMEM char usbHidReportDescriptor[USB_CFG_HID_REPORT_DESCRIPTOR_LENGTH] = { /* USB report descriptor */
    0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
    0x09, 0x06,                    // USAGE (Keyboard)
    0xa1, 0x01,                    // COLLECTION (Application)
    0x05, 0x07,                    // USAGE_PAGE (Keyboard)
    0x19, 0xe0,                    // USAGE_MINIMUM (Keyboard LeftControl)
    0x29, 0xe7,                    // USAGE_MAXIMUM (Keyboard Right GUI)
    0x15, 0x00,                    // LOGICAL_MINIMUM (0)
    0x25, 0x01,                    // LOGICAL_MAXIMUM (1)
    0x75, 0x01,                    // REPORT_SIZE (1)
    0x95, 0x08,                    // REPORT_COUNT (8)
    0x81, 0x02,                    // INPUT (Data,Var,Abs)
    0x95, 0x01,                    // REPORT_COUNT (simultaneous keystrokes)
    0x75, 0x08,                    // REPORT_SIZE (8)
    0x25, 0x65,                    // LOGICAL_MAXIMUM (101)
    0x19, 0x00,                    // USAGE_MINIMUM (Reserved (no event indicated))
    0x29, 0x65,                    // USAGE_MAXIMUM (Keyboard Application)
    0x81, 0x00,                    // INPUT (Data,Ary,Abs)
    0xc0                           // END_COLLECTION
};

static uchar report_buffer[2]; // buffer for HID reports (modifier, key press)

void keyboard_init() {
    cli();
    usbInit();
    usbDeviceDisconnect();
    _delay_ms(250);
    usbDeviceConnect();
    sei();

    // TODO: Remove the next two lines once we fix
    //       missing first keystroke bug properly.
    memset(report_buffer, 0, sizeof(report_buffer));
    usbSetInterrupt(report_buffer, sizeof(report_buffer));
}

void keyboard_update() {
    usbPoll();
}

// delay while updating until we are finished delaying
void keyboard_delay(long milli) {
    while (milli > 0) {
        _delay_ms(2);
        usbPoll();
        milli -= 2;
    }
}

// sends a key press only, with modifiers - no release
// to release the key, send again with key_press = 0
void keyboard_send_key_press(uint8_t key_press, uint8_t modifiers) {
    while (!usbInterruptIsReady()) {
        // Note: We wait until we can send key_press
        //       so we know the previous key_press was
        //       sent.
        usbPoll();
        _delay_ms(5);
    }

    memset(report_buffer, 0, sizeof(report_buffer));

    report_buffer[0] = modifiers;
    report_buffer[1] = key_press;

    usbSetInterrupt(report_buffer, sizeof(report_buffer));
}

// sends a key press AND release with modifiers
void keyboard_send_key_stroke(uint8_t key_stroke, uint8_t modifiers) {
    keyboard_send_key_press(key_stroke, modifiers);
    // This stops endlessly repeating keystrokes:
    keyboard_send_key_press(0, 0);
}

// USB_PUBLIC uchar usbFunctionSetup
uchar usbFunctionSetup(uchar data[8]) {
    usbRequest_t    *rq = (usbRequest_t *)((void *)data);

    usbMsgPtr = (usbMsgPtr_t) report_buffer;
    if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) {
        /* class request type */

        if (rq->bRequest == USBRQ_HID_GET_REPORT) {
            /* wValue: ReportType (highbyte), ReportID (lowbyte) */

            /* we only have one report type, so don't look at wValue */
            // TODO: Ensure it's okay not to return anything here?
            return 0;

        } else if (rq->bRequest == USBRQ_HID_GET_IDLE) {
            //usbMsgPtr = &idle_rate;
            //return 1;
            return 0;

        } else if (rq->bRequest == USBRQ_HID_SET_IDLE) {
            idle_rate = rq->wValue.bytes[1];

        }
    } else {
        /* no vendor specific requests implemented */
    }

    return 0;
}

#endif
