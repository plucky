(import scheme)
(import (chicken base))
(import (chicken process-context))
(import test)

(set-environment-variable! "TEST_MODE" "1")
(include "../plucky.scm")

(define (parse input) (parse-duckyscript (tokenize-duckyscript input)))
(define compile ast->sequence)

(define script "REM BASIC-style comment
//C++-style comment
DEFAULT_DELAY 500
DEFAULTDELAY 1000
   
STRING_DELAY 100 Type this slowly...
STRING Type this quickly!
DELAY 2000  
CTRL ALT DELETE
ALT-TAB
REPEAT 2
ENTER")

(define parse-tree
  '((default-delay 500)
    (default-delay 1000)
    (string-delay 100 "Type this slowly...")
    (string "Type this quickly!")
    (delay 2000)
    (combination ("CTRL" "ALT") "DELETE")
    (combination ("ALT") "TAB")
    (repeat 2)
    (combination () "ENTER")))

(test-group "Parsing all known commands and variations"
  (test parse-tree (parse script)))

(define (load-keymap name)
  (fill-keymap
   (try-read-layout
    (format "../duckyscript/layouts/~a" name))))

(define keymap (load-keymap 'us))
(define keymap-de (load-keymap 'de))
(define keymap-de-nodeadkeys (load-keymap 'de-nodeadkeys))

(test-group "Compiling string commands"
  (test '("KEY_A" () "KEY_B" () "KEY_C" ())
        (compile (parse "STRING abc") keymap))
  (test '("KEY_A" ("KEY_MOD_LSHIFT") "KEY_B" ("KEY_MOD_LSHIFT") "KEY_C" ("KEY_MOD_LSHIFT"))
        (compile (parse "STRING ABC") keymap)))

(test-group "Compiling commentary commands and blank lines"
  (test '() (compile (parse "REM comment\n\n//comment") keymap))
  (test '() (compile (parse "DELAY 0") keymap)))

(test-group "Compiling delay commands"
  (test '(0 200) (compile (parse "DELAY 200") keymap))
  (test '(0 255 0 245) (compile (parse "DELAY 500") keymap))
  (test '("KEY_A" () 0 100 "KEY_B" () 0 100 "KEY_C" () 0 100)
        (compile (parse "STRING_DELAY 100 abc") keymap)))

(test-group "Compiling default delay commands"
  (test '("KEY_A" () 0 100 "KEY_B" () 0 100 "KEY_C" () 0 100)
        (compile (parse "DEFAULT_DELAY 100\nSTRING a\nSTRING b\nSTRING c") keymap))
  (test '("KEY_X" () 0 200 0 100 "KEY_Y" () 0 200)
        (compile (parse "DEFAULT_DELAY 200\nSTRING x\nDELAY 100\nSTRING y") keymap)))

(test-group "Compiling repeat commands"
  (test-error (compile (parse "REPEAT 999") keymap))
  (test-error (compile (parse "REM\nREPEAT 999") keymap))
  (test-error (compile (parse "DEFAULT_DELAY 1\nREPEAT 999") keymap))
  (test '("KEY_1" ("KEY_MOD_LSHIFT") "KEY_1" ("KEY_MOD_LSHIFT") "KEY_1" ("KEY_MOD_LSHIFT"))
        (compile (parse "STRING !\nREPEAT 2") keymap))
  (test '("KEY_1" ("KEY_MOD_LSHIFT") "KEY_1" ("KEY_MOD_LSHIFT") "KEY_1" ("KEY_MOD_LSHIFT"))
        (compile (parse "STRING !\nREPEAT 1\nREPEAT 1") keymap)))

(test-group "Compiling combination commands"
  (test '("KEY_ENTER" ()) (compile (parse "ENTER") keymap))
  (test-error (compile (parse "r") keymap))
  (test '("KEY_SPACE" ()) (compile (parse "SPACE") keymap))
  (test '("KEY_ENTER" ("KEY_MOD_LMETA")) (compile (parse "GUI ENTER") keymap))
  (test '("KEY_R" ("KEY_MOD_LMETA")) (compile (parse "GUI r") keymap))
  (test '("KEY_R" ("KEY_MOD_LMETA")) (compile (parse "GUI R") keymap))
  (test '("KEY_DELETE" ("KEY_MOD_LCTRL" "KEY_MOD_LALT"))
        (compile (parse "CTRL ALT DELETE") keymap))
  (test '("KEY_LEFTMETA" ()) (compile (parse "WINDOWS") keymap)))

(test-group "Compiling string commands with different layouts"
  (test '("KEY_X" () "KEY_Y" () "KEY_Z" () "KEY_Z" () "KEY_Y" ())
        (compile (parse "STRING xyzzy") keymap))
  (test '("KEY_X" () "KEY_Z" () "KEY_Y" () "KEY_Y" () "KEY_Z" ())
        (compile (parse "STRING xyzzy") keymap-de))
  (test (compile (parse "STRING xyzzy") keymap)
        (compile (parse "STRING xzyyz") keymap-de))
  (test (compile (parse "STRING xyzzy") keymap-de)
        (compile (parse "STRING xzyyz") keymap)))

(test-group "Compiling string commands with and without dead keys"
  (test '("KEY_GRAVE" () "KEY_SPACE" ())
        (compile (parse "STRING ^") keymap-de))
  (test '("KEY_GRAVE" ())
        (compile (parse "STRING ^") keymap-de-nodeadkeys)))

(test-exit)
