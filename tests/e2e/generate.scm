(import scheme)
(import (chicken base))
(import (chicken file))
(import (chicken format))
(import (chicken pathname))
(import (chicken process-context))
(import (chicken string))
(import scsh-process)

(define (string-starts-with? prefix string)
  (let ((index (substring-index prefix string)))
    (and index (zero? index))))

(define (string-strip-prefix string prefix)
  (if (string-starts-with? prefix string)
      (substring string (string-length prefix))
      string))

(define (main)
  (when (> (length (command-line-arguments)) 2)
    (fprintf (current-error-port) "~a <encoder.jar> [testdir]\n" (program-name))
    (exit 1))
  (let ((encoder (car (command-line-arguments)))
        (testdir (if (null? (cdr (command-line-arguments)))
                     (current-directory)
                     (cadr (command-line-arguments))))
        (layouts (run/strings (plucky "--list-layouts"))))
    (find-files testdir test: ".*\\.duck" action:
                (lambda (path _)
                  (let* ((relative-path (string-strip-prefix path testdir))
                         (layout (car (string-split relative-path "/"))))
                    (when (member layout layouts)
                      (let ((out-path (pathname-replace-extension path ".bin")))
                        (printf "~a -> ~a\n" path out-path)
                        (run (java "-jar" ,encoder
                                   "-i" ,path
                                   "-o" ,out-path
                                   "-l" ,layout)))))))))

(main)
