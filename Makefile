CC = avr-gcc
OBJCOPY = avr-objcopy
TARGET_ARCH = -mmcu=attiny85
DEBUG ?= 0
CFLAGS = -Wall -Wextra -Os -DF_CPU=16500000 -DDEBUG=$(DEBUG) -Iusbdrv
OBJFLAGS = -j .text -j .data -O ihex
UPLOADER = micronucleus
TARGET = keyboard.hex

VPATH := usbdrv
OBJECTS := usbdrv.o usbdrvasm.o oddebug.o

.PHONY: all clean clear deploy

all: $(TARGET)
blink.elf: blink.o
keyboard.elf: $(OBJECTS) keyboard.o keyboard.h

%.o: %.S
	$(CC) $(CFLAGS) $(TARGET_ARCH) -x assembler-with-cpp -c $< -o $@

%.elf: %.o
	$(CC) $(CFLAGS) $(TARGET_ARCH) $^ -o $@

%.hex: %.elf
	$(OBJCOPY) $(OBJFLAGS) $< $@

clean:
	$(RM) $(OBJECTS) $(TARGET) $(TARGET:.hex=.o) $(TARGET:.hex=.elf)

clear:
	$(UPLOADER) --erase-only

deploy:
	$(UPLOADER) $(TARGET)
